(ns passport.events
  (:require
    [re-frame.core :as re-frame]
    [passport.db :as db]))


(re-frame/reg-event-db
  ::initialize-db
  (fn [_ _]
    db/default-db))

(re-frame/reg-event-db
  ::set-proof
  (fn [db [_ {headers :headers body :body signature :signature}]]
    (assoc-in db [:proof :content] {:headers headers :body body :signature signature})))


(re-frame/reg-event-fx
  ::file-selected
  (fn [cofx [_ file-name file]]
    (let [db (get cofx :db {})
          new-db (assoc-in db [:proof :uploaded-file] {:file-name file-name :file file})]
      {:db new-db :read-proof-from-image-file [file ::set-proof]})))