(ns passport.views
  (:require
    [re-frame.core :as re-frame]
    [passport.events :as events]
    [cljs.pprint :as pprint]
    [passport.subs :as subs]))

(defn select-file [evt]
  (let [target (.-target evt)
        file-name (.-value target)
        file (-> target .-files js->clj first js->clj)]
    (re-frame/dispatch [::events/file-selected file-name file])))

(defn input-file []
  [:div.field.is-horizontal
   [:div.field-label.is-normal
    [:label.label "Preuve de vaccination"]]
   [:div.field-body
    [:div.field
     [:input.input.field {:type      :file
                          :id        "file"
                          :on-change select-file}]]]])

(defn top-bar []
  [:nav.nav
   [:div.navbar-menu
    [:div.navbar-start
     [:a.navbar-item "Décodage de la preuve vaccinale"]]]])

(defn stupid-tables [records]
  (let [headers (reduce into (mapv keys records))]
    [:table.table.is-bordered
     [:thead
      [:tr
       (for [header headers] [:th {:key (name header)} (name header)])]]
     [:tbody
      (for [[idx record] (map-indexed vector records)]
        [:tr {:key idx} (for [header headers] [:td {:key (name header)} (str (get record header))])])]]))

(defn decoded-proof []
  (let [{headers :headers body :body signature :signature} @(re-frame/subscribe [::subs/proof-content])]
    [:div.my-3
     (when headers
       [:div.my-1
        [:h4.title.is-4 "Meta Informations"]
        [:div [stupid-tables [headers]]]])
     (when body
       [:div.my-1
        [:h4.title.is-4 "Body"]
        [:pre (with-out-str (pprint/pprint body))]])
     (when signature
       [:div.my-1
        [:h4.title.is-4 "Signature Cryptographique"]
        [:pre (str signature)]])]))

(defn footer []
  [:footer.footer
   [:div.content.has-text-centered
    (let [url "https://gitlab.com/didiercrunch/passport"
          link [:a {:href url} url]]
      [:p "Le code source de ce site web est accessible à " link "."])]])

(defn main-panel []
  [:div.columns.is-centered
   [:div.column.is-two-thirds
    [top-bar]
    [input-file]
    [:section
     [decoded-proof]]
    [:section
     [footer]]]])


