(ns passport.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
  ::name
  (fn [db]
    (:name db)))

(re-frame/reg-sub
  ::proof-content
  (fn [db]
    (get-in db [:proof :content])))

