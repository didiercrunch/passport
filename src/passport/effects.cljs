(ns passport.effects
  (:require [re-frame.core :as re-frame]
            [goog.json :as json]
            [goog.crypt.baseN :as bN]
            [goog.crypt.base64 :as b64]))

(defn partitionner [n]
  (let [count (atom -1)]
    (fn [m]
      (swap! count inc)
      (quot @count n))))

(defn js->keywordized-clj [o]
  (js->clj o :keywordize-keys true))

(defn parse-jwt [s]
  (let [[header body signature] (clojure.string/split s ".")
        base64-header (bN/recodeString header bN/BASE_64_URL_SAFE bN/BASE_64)
        base64-body (bN/recodeString body bN/BASE_64_URL_SAFE bN/BASE_64)]
    {:headers (->> base64-header b64/decodeString json/parse js->keywordized-clj)
     :signature signature
     :body  (->> base64-body
                 b64/decodeString
                 (.inflate js/RawDeflate)
                 json/parse
                 js->keywordized-clj)}))

(defn shc->str [encoded-str]
  (let [digit? (fn [c] (re-find #"\d" (str c)))
        to-char (fn [a] (->> a (apply str) js/parseInt (+ 45) char))
        xf (comp (filter digit?)
                 (partition-by (partitionner 2))
                 (map to-char))
        ret (transduce xf str encoded-str)]
    (parse-jwt ret)))

(defn- create-canvas []
  (let [canvas (. js/document createElement "canvas")
        hidden-div (. js/document getElementById "hidden")]
    (. hidden-div appendChild canvas)
    [canvas (. canvas getContext "2d")]))


(defn- get-qr-code [^js context]
  (let [height (.-height context)
        width (.-width context)
        options {}
        image-data (.-data (.getImageData context 0 0 width height))
        ret (js/jsQR image-data width height (clj->js options))]
    (js->keywordized-clj ret)))

(defn extract-proof-from-pdf-page [canvas context ^js page success-callback]
  (let [viewport  (.getViewport page (clj->js {:scale 1 :offsetX -50 :offsetY -185}))
        render-options {:canvasContext context :viewport viewport :intent "display" :background "white"}]
    (set! (.-width canvas) (- (.-width viewport) (* 2 50)))
    (set! (.-height canvas) (-  (.-height viewport) (* 2 185)))
    (set! (.-width context) (- (.-width viewport) (* 2 50)))
    (set! (.-height context) (-  (.-height viewport) (* 2 185)))
    (.then (.-promise (.render page (clj->js render-options)))
           #(success-callback (shc->str (:data (get-qr-code context)))))))

(defn extract-proof-from-pdf [canvas context ^js pdf success-callback]
  (.then (.getPage pdf 1)
         #(extract-proof-from-pdf-page canvas context % success-callback)))

(defn extract-proof-from-pdf-data [canvas context url success-callback]
  (.then  (.-promise (.getDocument js/pdfjsLib url))
          #(extract-proof-from-pdf canvas context % success-callback)))


(defn- extract-proof-from-image [canvas context image-url success-callback]
  (let [img (new js/Image)]
    (set! (. img -onload) (fn []
                            (set! (.-width context ) (.-width img))
                            (set! (.-width canvas) (.-width img))
                            (set! (.-height context) (.-height img))
                            (set! (.-height canvas) (.-height img))
                            (.drawImage context img 0 0)
                            (success-callback (shc->str (:data (get-qr-code context))))))
    (set! (. img -src) image-url)))

(defn- extract-proof-from-file [canvas context event success-callback]
  (let [url (.. event -target -result)]
    (cond
      (clojure.string/starts-with? url "data:image") (extract-proof-from-image canvas context url success-callback)
      (clojure.string/starts-with? url "data:application/pdf") (extract-proof-from-pdf-data canvas context url success-callback)
      :else (throw (js/Error. (str "Do not know how to parse file type."))))))

(defn dispatch-sucessful-read [event-name]
  (fn [payload]
    (re-frame/dispatch [event-name payload])))

(re-frame/reg-fx
  :read-proof-from-image-file
  (fn [[file success-event]]
    (let [file-reader (new js/FileReader)
          [canvas context] (create-canvas)
          success-callback (dispatch-sucessful-read success-event)]
      (.addEventListener file-reader "load" #(extract-proof-from-file canvas context % success-callback))
      (.readAsDataURL file-reader file))))

